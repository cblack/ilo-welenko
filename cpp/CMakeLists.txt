set(welenko_srcs
    main.cpp
)
set(welenko_headers)

set(REQUIRED_QT_VERSION 5.13.0)
find_package(Qt5 COMPONENTS Core Widgets Quick Concurrent REQUIRED)

add_executable(welenko
    ${welenko_srcs}
    ${welenko_headers}
)
add_dependencies(welenko
    welenko_go_lib
)
target_link_libraries(welenko
    welenko_go_lib pthread
    Qt5::Core Qt5::Widgets Qt5::Quick Qt5::Concurrent
)