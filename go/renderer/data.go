package renderer

const TemplateData = `
{{ define "text" }}
{{ if . }}
QQC2.Label {
	text: "{{ . }}"
	wrapMode: Text.WordWrap

	Layout.fillWidth: true
}
{{ end }}
{{ end }}
` + `
{{ define "heading1" }}
{{ if . }}
Kirigami.Heading {
	text: "{{ . }}"
	wrapMode: Text.WordWrap

	Layout.fillWidth: true
}
{{ end }}
{{ end }}
` + `
{{ define "heading2" }}
{{ if . }}
Kirigami.Heading {
	level: 2

	text: "{{ . }}"
	wrapMode: Text.WordWrap

	Layout.fillWidth: true
}
{{ end }}
{{ end }}
` + `
{{ define "heading3" }}
{{ if . }}
Kirigami.Heading {
	level: 3

	text: "{{ . }}"
	wrapMode: Text.WordWrap

	Layout.fillWidth: true
}
{{ end }}
{{ end }}
` + `
{{ define "heading4" }}
{{ if . }}
Kirigami.Heading {
	level: 4

	text: "{{ . }}"
	wrapMode: Text.WordWrap

	Layout.fillWidth: true
}
{{ end }}
{{ end }}
` + `
{{ define "heading5" }}
{{ if . }}
Kirigami.Heading {
	level: 5

	text: "{{ . }}"
	wrapMode: Text.WordWrap

	Layout.fillWidth: true
}
{{ end }}
{{ end }}
` + `
{{ define "" }}
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12 as QQC2
import org.kde.kirigami 2.12 as Kirigami

Kirigami.ApplicationWindow {
title: "Readway Desktop"
pageStack.initialPage: Kirigami.ScrollablePage {
	title: "{{ .Name }}"
	ColumnLayout {
		ColumnLayout {
			Layout.fillWidth: true
			Layout.maximumWidth: 800
			Layout.alignment: Qt.AlignHCenter

			{{ range $iface := .Interfaces }}
			Kirigami.Card {
				implicitWidth: {{ (print $iface.Anchor ".width") }}
				implicitHeight: {{ (print $iface.Anchor ".height") }}
				ColumnLayout {
					id: {{ $iface.Anchor }}
					{{ template "heading1" $iface.Name }}
					{{ template "heading5" (print "Version " $iface.Version) }}
				}
			}
			{{ end }}
		}
	}
}
}
{{ end }}
`
