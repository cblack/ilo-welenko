package renderer

import (
	"log"
	"strings"
	"text/template"
)

func RenderProtocolToString(protocol Protocol) (string, bool) {
	tmpl, err := template.New("").Parse(TemplateData)
	if err != nil {
		panic(err)
	}
	sb := strings.Builder{}
	err = tmpl.Execute(&sb, protocol)
	if err != nil {
		log.Println(err.Error())
		return "", false
	}
	return sb.String(), true
}
