package renderer

import (
	"encoding/xml"
	"io/ioutil"
	"log"
	"math/rand"
	"time"
)

type Description struct {
	Summary string `xml:"summary,attr"`
	Body    string `xml:",chardata"`
}

type Anchorer struct {
	ID string
}

const charset = "abcdefghijklmnopqrstuvwxyz_"

var seededRand *rand.Rand = rand.New(
	rand.NewSource(time.Now().UnixNano()))

func StringWithCharset(length int, charset string) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

func String(length int) string {
	return StringWithCharset(length, charset)
}

func (a *Anchorer) Anchor() string {
	if a.ID == "" {
		a.ID = String(256)
	}
	return a.ID
}

type Argument struct {
	Anchorer
	Description Description `xml:"description"`
	Name        string      `xml:"name,attr"`
	Type        string      `xml:"type,attr"`
	Summary     string      `xml:"summary,attr"`
	Interface   string      `xml:"interface,attr"`
	AllowNull   bool        `xml:"allow-null,attr"`
	Enum        string      `xml:"enum,attr"`
}

func (a *Argument) Anchor() string {
	return a.Anchorer.Anchor()
}

type Request struct {
	Anchorer
	Description Description `xml:"description"`
	Arguments   []Argument  `xml:"arg"`
	Name        string      `xml:"name,attr"`
	Type        string      `xml:"type,attr"`
	Since       int         `xml:"since,attr"`
}

func (a *Request) Anchor() string {
	return a.Anchorer.Anchor()
}

type Event struct {
	Anchorer
	Description Description `xml:"description"`
	Arguments   []Argument  `xml:"arg"`
	Name        string      `xml:"name,attr"`
	Since       int         `xml:"since,attr"`
}

func (a *Event) Anchor() string {
	return a.Anchorer.Anchor()
}

type EnumEntry struct {
	Anchorer
	Description Description `xml:"description"`
	Name        string      `xml:"name,attr"`
	Summary     string      `xml:"summary,attr"`
	Value       string      `xml:"value,attr"`
	Since       int         `xml:"since,attr"`
}

func (a *EnumEntry) Anchor() string {
	return a.Anchorer.Anchor()
}

type Enum struct {
	Anchorer
	Description Description `xml:"description"`
	Entries     []EnumEntry `xml:"entry"`
	Name        string      `xml:"name,attr"`
	Since       int         `xml:"since,attr"`
	Bitfield    bool        `xml:"bitfield,attr"`
}

func (a *Enum) Anchor() string {
	return a.Anchorer.Anchor()
}

type Interface struct {
	Anchorer
	Name        string      `xml:"name,attr"`
	Version     int         `xml:"version,attr"`
	Description Description `xml:"description"`
	Requests    []Request   `xml:"request"`
	Events      []Event     `xml:"event"`
	Enums       []Enum      `xml:"enum"`
}

func (a *Interface) Anchor() string {
	return a.Anchorer.Anchor()
}

type Protocol struct {
	Anchorer
	Name        string      `xml:"name,attr"`
	Copyright   string      `xml:"copyright"`
	Description Description `xml:"description"`
	Interfaces  []Interface `xml:"interface"`
}

func (a *Protocol) Anchor() string {
	return a.Anchorer.Anchor()
}

func LoadProtocol(path string) (Protocol, bool) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		log.Println(err.Error())
		return Protocol{}, false
	}
	proto := Protocol{}
	err = xml.Unmarshal(data, &proto)
	if err != nil {
		log.Println(err.Error())
		return Protocol{}, false
	}
	return proto, true
}
