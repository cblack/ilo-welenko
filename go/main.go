package main

import "welenko/renderer"
import "C"

var empty = C.CString("")

//export Render
func Render(path *C.char) (*C.char, bool) {
	proto, ok := renderer.LoadProtocol(C.GoString(path))
	if !ok {
		return empty, false
	}
	data, ok := renderer.RenderProtocolToString(proto)
	if !ok {
		return empty, false
	}
	return C.CString(data), true
}

func main() {}
